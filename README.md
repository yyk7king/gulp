# gitlab pages 연동
- 별도 도메인 없이 본인 gitlab 아이디로 작업한 html 파일을 확인 할 수 있다
- ex> 본인아이디.gitlab.io/레파지토리(Repository) 명/브렌치(branch) 명/폴더 구조/*.html
- ex> 나의 경우 yyk7king.gitlab.io/gulp/v1/html/index.html 으로 외부에서 누구나 확인 가능하다

솔직히 gitlab에 들어가서 어디가서 setting 누르고 어쩌고 저쩌고 나는 잘 모르겠다.<br>
그냥 이렇게 하면 뽝 하고 되는게 좋은데...음...기능도 많은데 어디가 어딘지 모르겠다....<br>
그래서 정리 해 본다.<br>
1. gitlab 아이디 만들기 - 이건 회원가입 하면 된다
2. Repository 생성
    - 회원가입 후 gitlab에서 생성하면 된다(쉽게 말하면 폴더 만드는거 같은거다)
    - 프로젝트 마다 관리를 해야 하니 폴더명은 본인들 마음대로
3. ".gitlab-ci.yml" 파일 본인이 직접 생성
    - 본인이 만든 Repository에 만든다
    - 나같은 경우 Repository를 gulp로 만들었으니까 /gulp/.gitlab-ci.yml 파일이 존재한다
<br>
.gitlab-ci.yml 파일 내용 - branch 하나만 pages 연동할때
<code>
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
</code>

<br>
.gitlab-ci.yml 파일 내용 - 여러개 branch를 각각 pages 연동할때
<code>
pages:
  script:
    - echo $CI_COMMIT_REF_NAME
    - if [[ "$CI_COMMIT_REF_NAME" = "master" ]]; then
        echo "This is master branch";
        mkdir -p public/master;
        rm -rf public/master/*;
        mv src/* public/master;
      elif [[ "$CI_COMMIT_REF_NAME" = "v1" ]]; then
        echo "This is v1 branch";
        mkdir -p public/v1;
        rm -rf public/v1/*;
        mv src/* public/v1;
      fi;
  cache:
    paths:
      - public
  only:
    - master
    - v1
  artifacts:
    paths:
      - public
</code>
복사 붙여 넣기 하자<br><br>
4. gitlab에 add, commit, push 순으로 올리자 - 모르면 검색ㄱㄱ<br>
5. 위에 예시처럼 본인 id.gitlab.io/Repository(레파지토리) 폴더명/본인이 작성한 폴더 경로/*.html를 브라우져 주소창에 입력해서 확인하자

# gulp 설치

